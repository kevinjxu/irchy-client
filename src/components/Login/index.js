import React from "react";
import settings from "electron-settings";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Redirect } from "react-router-dom";
import { authorized,authing } from "../../redux/actions/settings";
import CreateProduct from "../CreateProduct";
import path from "path";

class Login extends React.Component {
	constructor(props) {
		super(props);
	}
	toggleCreateForm() {
		const createPage = document.getElementsByClassName("createteam")[0];
		createPage.classList.toggle("createteam-hide");
		if (!createPage.classList.contains("createteam-hide")) {
			document.getElementById("name").focus();
		}
	}
	render() {
		if (this.props.AUTH) {
			return (<Redirect to="/projectselection"/>);
		}
		return (
			<div id="login">
				{this.props.AUTHING && <div className="authing">
					<i id="auth-spinner" className="iconfont icon-laoshu"></i>
					<span id="auth-spinner-text"></span>
				</div> }
				<form id="login-form" action="javascript:void(0)" onSubmit={(e)=>{settings.set("uid", e.target.username.value);e.preventDefault();this.props.authing(true); setTimeout(() => {
					this.props.authorized(true);
				}, 200);}}>
					<img width="75px" height="75px" src={"static/icon/irchy.ico"} alt=""/>
					<h1 id="app-name">irchy</h1>
					<div className="login-fields">
						<i id="uid-icon" className="iconfont icon-zhanghao"></i>
						<input ref={input=>{settings.get("uid") ? undefined : input && input.focus()}} type="email" name="username" id="uid" className="login-field" placeholder="email" defaultValue={settings.get("uid")} required/>
					</div>
					<div className="login-fields">
						<i id="pw-icon" className="iconfont icon-mima"></i>
						<input ref={input=>{settings.get("uid") ? input && input.focus() : undefined}} type="password" name="password" id="pw" className="login-field" placeholder="password" required/>
					</div>
					<button type="submit" id="login-btn">LOG IN</button>
					<input onClick={this.toggleCreateForm} type="button" id="create-team-btn" value="+ CREATE PRODUCT"/>
				</form>
				<CreateProduct/>
			</div>
		);
	}
}


export default connect(
	(state) => ({AUTH: state.settings.authorized, AUTHING: state.settings.authing}),
	(dispatch) => bindActionCreators({authorized, authing}, dispatch)
)(Login);