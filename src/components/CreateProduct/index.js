import React from "react";

export default class CreateProduct extends React.Component {
	constructor(props) {
		super(props);
		this.displayLogo = this.displayLogo.bind(this);
	}
	goBack() {
		const createPage = document.getElementsByClassName("createteam")[0];
		const uid = document.getElementById("uid");
		if (!createPage.classList.contains("createteam-hide")) {
			createPage.classList.toggle("createteam-hide");
			uid.value ? document.getElementById("pw").focus() : uid.focus();
		}
	}
	hasExtension(target, exts) {
		return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$', "i")).test(target);
	}
	displayLogo(e) {
		if (e.target.files[0] && this.hasExtension(e.target.files[0].name, ["jpg", "jpeg", "png", "svg"])) {
			const selectedLogo = document.getElementById("selected-logo");
			const logo = document.getElementById("logo");
			selectedLogo.style.display = "flex";
			logo.src = e.target.files[0].path;
		}
		else if (e.target.files[0]) {
			alert("Logo must be an image file!");
			e.target.value = "";
		}
	}
	render() {
		return (
			<div className="createteam createteam-hide">
				<div onClick={this.goBack} className="create-back-btn"><i id="back-arrow" className="iconfont icon-xaingzuo"></i><span className="back-login">Back</span></div>
				<div className="create-form-container">
					<form id="create-form" action="javascript:void(0)" onSubmit={()=>{console.log("Create Team")}}>
						<h1>Whale-com to irchy</h1>
						<h1 className="create-logo"><i id="jingyu" className="iconfont icon-jingyu"></i></h1>
						<div className="create-fields">
							<span id="name-icon">Name</span>
							<input id="name" type="text" name="name" className="create-field" placeholder="What's Your Mom Call You?" required/>
						</div>
						<div className="create-fields">
							<span id="email-icon">Email</span>
							<input type="email" name="email" className="create-field" placeholder="Desired Email for Verification?" required/>
						</div>
						<div className="create-fields">
							<span id="product-icon">Product</span>
							<input type="text" name="productname" className="create-field" placeholder="What Are You Making?" required/>
						</div>
						<div className="create-fields file-drop-container">
							<div id="selected-logo">
								<img id="logo" src="#" alt=""/>
							</div>
							<i id="file-icon" className="iconfont icon-upload"></i>
							<h4>Drop Your Logo Here!</h4>
							<p>You can drop any jpg, png, jpeg, svg files.</p>
							<input id="file-selector" type="file" name="productlogo" className="file-field" accept="image/*" onChange={this.displayLogo} required/>
						</div>
						<button type="submit" id="create-btn">CREATE</button>
					</form>
				</div>
			</div>
		);
	}
}