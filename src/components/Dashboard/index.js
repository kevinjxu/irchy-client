import React from "react";
import {NavLink} from "react-router-dom";
import Path from "path";
import {MainRoutes} from "../../routes";

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="dashboard">
				<img id="avatar" src={Path.resolve("./static/imgs/avatar.jpg")} alt="Avatar"/>
				<div className="d-links">
					{MainRoutes.map((route, index)=>{
							if (!route.once) {
								return <NavLink 
								key={index}
								exact={route.exact}
								to={route.path}
								className="d-nav"
								activeClassName="d-nav-active">{route.label}</NavLink>;
							}
						})}
				</div>
			</div>
		);
	}
}