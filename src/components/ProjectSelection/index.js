import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Main from "../Main";
import AddProjectWindow from "../AddProject";
import { Redirect, NavLink } from "react-router-dom";
import { setCurrentProject } from "../../redux/actions/user";

const Project = props => (
	<div onClick={()=>{props.select(props.projectindex)}} className="project">
		<img className="p-logo" src={props.logo} alt="X"/>
		<h1 className="p-name">{props.productName}</h1>
		<span className="p-owner">{props.owner}</span>
	</div>
);

class ProjectSelection extends React.Component {
	constructor(props) {
		super(props);
		this.addProject = this.addProject.bind(this);
		this.selectProject = this.selectProject.bind(this);
	}
	selectProject(index) {
		this.props.setCurrentProject(this.props.user.projects[index]);
	}
	addProject(e) {
		const addProjectWindow = document.getElementsByClassName("add-project-window-active")[0];
		addProjectWindow.classList.toggle("add-project-window-hide");
	}
	render() {
		if (this.props.user.currentProject) {
			return (<Redirect to="/authorized/notifications"/>);
		}
		return (
			<div id="projects">
				<div className="project-list">
					{this.props.user.projects.map((elem, index)=>(
						<Project key={index} productName={elem.name} owner={elem.owner} logo={elem.logo} projectindex={index} select={this.selectProject}/>
					))}
				</div>
				<div className="add-project" id="add-project-btn" onClick={this.addProject}>
					<h4 id="add-project-text">+ Add Project</h4>
					<div onClick={this.addProject} className="add-project-window-active add-project-window-hide">
						<AddProjectWindow close={this.addProject}/>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(
	(state) => ({user: state.user}),
	(dispatch) => bindActionCreators({setCurrentProject}, dispatch),
	null,
	{pure: false}
)(ProjectSelection);
