import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { newProduct } from "../../redux/actions/user";

class AddProduct extends React.Component {
	constructor(props) {
		super(props);
		this.displayLogo = this.displayLogo.bind(this);
		this.newProduct = this.newProduct.bind(this);
	}
	hasExtension(target, exts) {
		return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$', "i")).test(target);
	}
	displayLogo(e) {
		if (e.target.files[0] && this.hasExtension(e.target.files[0].name, ["jpg", "jpeg", "png", "svg"])) {
			const selectedLogo = document.getElementById("selected-logo");
			const logo = document.getElementById("product-logo");
			selectedLogo.style.display = "flex";
			logo.src = e.target.files[0].path;
		}
		else if (e.target.files[0]) {
			alert("Logo must be an image file!");
			e.target.value = "";
		}
	}
	close() {
		const addProjectWindow = document.getElementsByClassName("add-project-window-active")[0];
		!addProjectWindow.classList.contains("add-project-window-hide") ? addProjectWindow.classList.add("add-project-window-hide") : undefined;
	}
	newProduct(e) {
		this.props.newProduct(e.target.pname.value, e.target.productlogo.files[0]);
	}
	render() {
		return (
			<div className="add-product-container">
				<div onClick={this.close} className="close-btn-container"><i id="close-btn" className="iconfont icon-shanchudelete30"></i></div>
				<div className="add-product-form-container">
					<form className="add-product-form" action="javascript:void(0)" onSubmit={this.newProduct}>
						<h1><i id="add-product-icon" className="iconfont icon-wodechanpin"></i></h1>
						<div className="add-product-fields">
							<input type="text" name="pname" id="add-product-name" placeholder="Product Name"/>
						</div>
						<div className="add-product-fields">
							<textarea name="description" id="add-product-description" cols="20" rows="10"></textarea>
						</div>
						<div className="add-product-fields file-drop-container">
							<div id="selected-logo">
								<img id="product-logo" src="#" alt=""/>
							</div>
							<i id="file-icon" className="iconfont icon-upload"></i>
							<h4>Drop Your Logo Here!</h4>
							<p>You can drop any jpg, png, jpeg, svg files.</p>
							<input id="file-selector" type="file" name="productlogo" className="file-field" accept="image/*" onChange={this.displayLogo} required/>
						</div>
						<button type="submit" id="add-product-btn">ADD</button>
					</form>
				</div>
			</div>
		);
	}
}

export default connect(
	(state) => ({}),
	(dispatch) => bindActionCreators({newProduct}, dispatch)
)(AddProduct);