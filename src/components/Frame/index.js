import React from "react";
import { remote } from "electron";

const Frame = props => (
	<div id="frame">
		<ul className="frame-list">
			<li 
				onClick={()=>{remote.getCurrentWindow().close()}} 
				id="closebtn" 
				className="frame-control">
				X
			</li>
			<li 
				onClick={()=>{remote.getCurrentWindow().minimize()}} 
				id="minbtn" 
				className="frame-control">
				-
			</li>
			<li 
				onClick={()=>{
					let win = remote.getCurrentWindow();
					win.setFullScreen(win.isFullScreen() ? false : true);
				}} 
				id="resbtn" 
				className="frame-control">
				+
			</li>
		</ul>
	</div>	
);

export default Frame;