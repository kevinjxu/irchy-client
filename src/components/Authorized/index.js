import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { MainRoutes, RouteWithSubRoutes } from "../../routes";

class Authorized extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="authorized">
				{MainRoutes.map((elem, index)=>(
					<RouteWithSubRoutes key={index} exact={elem.exact} {...elem} {...this.props}/>
				))}
			</div>
		)
	}
}

export default connect(
	(state) => ({user: state.user}),
	(dispatch) => bindActionCreators({}, dispatch),
	null,
	{pure: false}
)(Authorized);