import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Redirect, Route } from "react-router-dom";
import Dashboard from "../Dashboard";
import {MainRoutes} from "../../routes";


class Main extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="main">
				<Dashboard/>
				{MainRoutes.map((route, index)=>{
					if (!route.once) {
						return <Route key={index} exact={route.exact} path={route.path} component={route.component}/>;
					}
				})}
			</div>
		)
	}
}

export default connect(
	(state) => ({user: state.user}),
	(dispatch) => bindActionCreators({}, dispatch)
)(Main);