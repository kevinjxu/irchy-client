export default {
	CN: {
		noti: "通知",
		tasks: "任务",
		project: "产品",
		message: "信息",
		documents: "文件",
		settings: "设置",
		team: "团队",
		account: "个人",
		general: "基本",
		overview: "簡介",
		wall: "任务",
		logs: "日志",
		urgent: "紧急",
		important: "重要",
		bug: "Bug",
		fix: "Fix"
	},
	EN: {
		noti: "Notifications",
		tasks: "Tasks",
		project: "Project",
		message: "Messages",
		documents: "Documents",
		settings: "Settings",
		team: "Team",
		account: "Account",
		general: "General",
		overview: "Overview",
		wall: "Wall",
		logs: "Logs",
		bug: "Bug",
		fix: "Fix"
	}
}