import React from "react";
import { Route } from "react-router-dom";
import ProjectSelection from "../components/ProjectSelection";
import Login from "../components/Login";
import Private from "./private";
import Main from "../components/Main";
import Noti from "../components/Notification";

export const RouteWithSubRoutes = route => {
	return (
		<Route path={route.path} exact={route.exact} render={props=>{
			return <route.component {...props} routes={route.routes} />;
		}}/>
	)
}

export const MainRoutes = [
	{
		label: "Project Selection",
		path: "/authorized/projectselection",
		exact: true,
		once: true,
		component: ProjectSelection	
	},
	{
		label: "Notifications",
		path: "/authorized/notifications",
		component: Noti
	},
	{
		label: "Project",
		path: "/authorized/project",
		component: null,
		subroutes: [
			{
				label: "Overview",
				path: "/authorized/project",
				component: null
			},
			{
				label: "Wall",
				path: "/authorized/project/wall",
				component: null
			},
			{
				label: "Logs",
				path: "/authorized/project/logs",
				component: null
			}
		]
	},
	{
		label: "Discussion",
		path: "/authorized/discussion",
		component: null
	},
	{
		label: "Tasks",
		path: "/authorized/tasks",
		component: null
	},
	{
		label: "Documents",
		path: "/authorized/documents",
		component: null
	},
	{
		label: "Settings",
		path: "/authorized/settings",
		component: null
	},
	{
		label: "Log out",
		path: "/login",
		component: Login
	}
]