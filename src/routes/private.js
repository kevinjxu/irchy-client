import React from "react";
import { Route, Redirect } from "react-router-dom"; // eslint-disable-line no-unused-vars
import { connect } from "react-redux";
import Proptypes from "prop-types";


class PrivateRoute extends React.Component{
	constructor(props) {
		super(props);
	}
	render() {
		const {component: Component, ...rest} = this.props; // eslint-disable-line no-unused-vars
		const Path = this.props.path;
		const redirectPath = this.props.redirect;
		return (
			<Route {...rest} render={props=>(
				rest.authState ? (
					<Component {...props}/>
				) : ( <Redirect to={{
					pathname: redirectPath,
					redirect: Path
				}}/>
				)
			)}/>
		);
	}
}

export default connect(
	(state) => ({authState: state.settings.authorized}),
	null,null,{pure: false}
)(PrivateRoute);
