// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
import React from "react";
import {render} from "react-dom";
import store from "./redux";
import Login from "./components/Login";
import Frame from "./components/Frame"; // eslint-disable-line no-unused-vars
import Main from "./components/Main"; // eslint-disable-line no-unused-vars
import PrivateRoute from "./routes/private"; // eslint-disable-line no-unused-vars
import { Provider } from "react-redux"; // eslint-disable-line no-unused-vars
import { MemoryRouter, Route } from "react-router-dom"; // eslint-disable-line no-unused-vars
import ProjectSelection from "./components/ProjectSelection";

export default class App extends React.Component{
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<MemoryRouter initialEntries={["/authorized"]} initialIndex={0}>
				<div className="root">
					<Frame/>
					<Route exact path="/login" component={Login}/>
					<PrivateRoute path="/authorized" redirect="/login" component={Main}/>
					<Route exact path="/projectselection" component={ProjectSelection}/>
				</div>
			</MemoryRouter>
		);
	}
}

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("app")
);