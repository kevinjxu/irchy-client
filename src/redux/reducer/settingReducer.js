/****************************************************************************
 * 
 *->File Name: authReducer.js
 *->File Purpose: To declare a reducer for authenticating.
 *
*****************************************************************************/

import Types from "../types";
/****************************************************************************
 * 
 *->Object Name: initialState
 *->Object Purpose: To set the initial state of the store.
 *
*****************************************************************************/
const initialState = {
	notification: true,
	language: "english",
	theme: "light",
	authorized: false,
	fetching: false,
	fetched: true,
	authing: false,
	registered: false,
	registering: false,
	error: undefined
};

const settingReducer = (state=initialState, action) => {
	switch(action.type) {
	case Types.State.Auth.SET_AUTHING:
		return Object.assign({}, state, {
			authing: action.value
		});
	case Types.State.Auth.SET_AUTHORIZED:
		return Object.assign({}, state, {
			authorized: action.value
		});
	case Types.Settings.General.SET_LANGUAGE:
		return Object.assign({}, state, {
			language: action.value
		});
	case Types.Settings.General.SET_THEME:
		return Object.assign({}, state, {
			theme: action.value
		});
	case Types.Settings.General.SET_NOTIFICATION:
		return Object.assign({}, state, {
			notification: action.value
		});
	
	default:
		return Object.assign({}, state);
	}
};

export default settingReducer;
