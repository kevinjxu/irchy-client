/****************************************************************************
 * 
 *->File Name: authReducer.js
 *->File Purpose: To declare a reducer for authenticating.
 *
*****************************************************************************/

import Types from "../types";
/****************************************************************************
 * 
 *->Object Name: initialState
 *->Object Purpose: To set the initial state of the store.
 *
*****************************************************************************/
const initialState = {
	uid: undefined,
	firstname: undefined,
	lastname: undefined,
	avatar: undefined,
	email: undefined,
	position: undefined,
	token: undefined,
	currentProject: undefined,
	projects: [
		{
			id: 0,
			name: "Bacon",
			logo: undefined,
			owner: "Kevin"
		}
	]
};

const userReducer = (state=initialState, action) => {
	switch(action.type) {
	case Types.User.Projects.NEW_PROJECT:
		return Object.assign({}, state, {
			projects: [...state.projects, action.value]
		});
	case Types.User.Projects.SET_PROJECT:
		return Object.assign({}, state, {
			currentProject: action.value
		});
	default:
		return Object.assign({}, state);
	}
};

export default userReducer;
