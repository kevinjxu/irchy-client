import Types from "../types";

export function newProduct(pname, logo) {
	return async dispatch=>{
		dispatch(addProject({name: pname, logo: logo.path, owner: "Me", id: 0}));
	}
}

export function addProject(product) {
	return {
		type:  Types.User.Projects.NEW_PROJECT,
		value: product
	}
}

export function setCurrentProject(project) {
	return {
		type: Types.User.Projects.SET_PROJECT,
		value: project
	}
}