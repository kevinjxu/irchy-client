import Types from "../types";

export function authing(status)
{
    return {
        type: Types.State.Auth.SET_AUTHING,
        value: status
    }
}

export function authorized(status)
{
    return {
        type: Types.State.Auth.SET_AUTHORIZED,
        value: status
    }
}