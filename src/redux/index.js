/****************************************************************************
 * 
 *->Name: Store.js 
 *->Purpose: Declare the Redux Store/State tree and Reducer.
 *
*****************************************************************************/

import {createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import userReducer from "./reducer/userReducer";
import settingReducer from "./reducer/settingReducer";
import { combineReducers } from "redux";

/****************************************************************************
 * 
 *->Reducer Name: reducer
 *->Reducer Purpose: To handle the actions of User login.
 *
*****************************************************************************/
const reducer = combineReducers({
	user: userReducer,
	settings: settingReducer
});

/****************************************************************************
 * 
 *->Function Name: middleware
 *->Function Purpose: To apply the thunk middleware to redux, allowing functions to be dispatched.
 *
*****************************************************************************/
const middleware = applyMiddleware(thunk);

export default createStore(reducer, middleware);