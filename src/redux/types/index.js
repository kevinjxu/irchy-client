export default {
	User: {
		Projects: {
			NEW_PROJECT: "NEW_PROJECT",
			SET_PROJECT: "SET_PROJECT"
		},
		Notifications: {
			SET_NOTIFICATION: "",
			LIVE_NOTIFICATION: "",
		}
	},
	State: {
		Auth: {
			SET_AUTHORIZED: "SET_AUTHORIZED",
			SET_AUTHING: "SET_AUTHING",
			SET_REGISTED: "",
			SET_REGISTERING: ""
		},
		Fetch: {
			SET_FETCHING: "",
			SET_FETCHED: "",
		}
	},
	Settings: {
		General: {
			SET_THEME: "",
			SET_NOTIFICATION: "",
			SET_LANGUAGE: ""
		},
		Account: {
			SET_FNAME: "",
			SET_LNAME: "",
			SET_AVATAR: "",
			SET_POSITION: ""
		},
		Team: {
			GEN_SHARE_LINK: "",
			SET_TEAM_MEMBERS: "",
		},
		Project: {
			SET_PROJECT_NAME: "",
			SET_PROJECT_LOGO: "",
			SET_PROJECT_DESCRIPTION: "",
			SET_PROJECT_URL: "",
			SET_PROJECT_PRIVACY: ""
		},
		Error: {
			SET_ERROR: ""
		}
	}
}